module.exports = {
  sanity: {
    projectId: process.env.GATSBY_SANITY_PROJECT_ID || 'ul9znuia',
    dataset: process.env.GATSBY_SANITY_DATASET || 'production'
  }
}
