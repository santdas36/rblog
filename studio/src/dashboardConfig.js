export default {
  widgets: [
    { name: 'structure-menu' },
    {
      name: 'project-info',
      options: {
        __experimental_before: [
          {
            name: 'netlify',
            options: {
              description:
                'NOTE: Because these sites are static builds, they need to be re-deployed to see the changes when documents are published.',
              sites: [
                {
                  buildHookId: '5e6da4731fbec3c329a262c5',
                  title: 'Sanity Studio',
                  name: 'rblog-studio',
                  apiId: 'b5754e40-84a3-41d9-8a57-8b043311e1bf'
                },
                {
                  buildHookId: '5e6da473f656cc6ef383ef27',
                  title: 'Blog Website',
                  name: 'rblog-web',
                  apiId: 'c379ee17-358e-4813-a66d-b30206ad5e40'
                }
              ]
            }
          }
        ],
        data: [
          {
            title: 'GitHub repo',
            value: 'https://github.com/santdas36/rblog',
            category: 'Code'
          },
          { title: 'Frontend', value: 'https://rblog-web.netlify.com', category: 'apps' }
        ]
      }
    },
    { name: 'project-users', layout: { height: 'auto' } },
    {
      name: 'document-list',
      options: { title: 'Recent blog posts', order: '_createdAt desc', types: ['post'] },
      layout: { width: 'medium' }
    }
  ]
}
